package com.sutaitech.action;

import java.beans.PropertyEditorSupport;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import com.sutaitech.common.Constant;
import com.sutaitech.utils.CookieUtil;

@Controller
public class BaseAction {
    
    /**
     * 取当前登录用户名
     * @param request
     * @param response
     * @return
     */
    public String getUserName(HttpServletRequest request, HttpServletResponse response) {
        return CookieUtil.getCookie(request, response, Constant.COOKIE_LOGIN_NAME_KEY);
    }
    
    /**
     * 取当前登录用户名
     * @param request
     * @param response
     * @return
     */
    public String getUserType(HttpServletRequest request, HttpServletResponse response) {
        return CookieUtil.getCookie(request, response, Constant.COOKIE_LOGIN_TYPE_KEY);
    }
    
    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        dataBinder.registerCustomEditor(String.class, new PropertyEditorSupport() {
            public void setAsText(String value) {
                try {
                    if (value != null) {
                        setValue(value.trim());
                    } else {
                        setValue(null);
                    }
                } catch (Exception e) {
                    setValue(null);
                }
            }
        });
        dataBinder.registerCustomEditor(Date.class, new PropertyEditorSupport() {
            public void setAsText(String value) {
                try {
                    if (StringUtils.isNotBlank(value) && value.contains(":")) {
                        setValue(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(value));
                    } else if (StringUtils.isNotBlank(value)) {
                        setValue(new SimpleDateFormat("yyyy-MM-dd").parse(value));
                    } else {
                        setValue(null);
                    }
                } catch (ParseException e) {
                    setValue(null);
                }
            }
        });
    }

}
